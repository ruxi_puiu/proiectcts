package org.nqc.bugme.data.storage;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConsoleTestStorageListenerTest.class, InMemoryTestStorageTest.class, ObjectDBStorageTest.class})
public class StorageTestSuite {

}
