/**
 * 
 */
package org.nqc.bugme.data.storage;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.nqc.bugme.data.PersonID;
import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.TicketResponse;
import org.nqc.bugme.data.storage.query.TicketRequestQueryCriteria;

/**
 * @author adch
 *
 */
public class ObjectDBStorageTest {


	private static final String TEMP_STORAGE_DB = "/tempStorage.db";




	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		try {
			Files.delete(FileSystems.getDefault().getPath(TEMP_STORAGE_DB));
			System.out.println("Deleted file " + TEMP_STORAGE_DB);
		} catch (IOException e1) {
			//e1.printStackTrace();
			System.err.println("Failed to delete file " + TEMP_STORAGE_DB + " due to:" + e1);
		}
		
		
		try {
			ObjectDBStorage storage = new ObjectDBStorage(TEMP_STORAGE_DB);
			System.out.println("Created temporary storage " + TEMP_STORAGE_DB);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try {
			Files.delete(FileSystems.getDefault().getPath(TEMP_STORAGE_DB));
			System.out.println("Final cleanup: deleted file " + TEMP_STORAGE_DB);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Test method for {@link org.nqc.bugme.data.storage.ObjectDBStorage#addResponse(TicketResponse)}.
	 */
	@Test
	public void testAddResponse() {
		
		ObjectDBStorage storage = new ObjectDBStorage(TEMP_STORAGE_DB);
		try {
			TicketRequest r = new TicketRequest("Storage1", "This is the first storage", new PersonID("Ionel"));
			TicketResponse response = new TicketResponse(r, null, "This is the first response to the first storage", "Response1 - PASSED", true);
			r.addResponse(response);
			storage.addRequest(r);
			
			
			// now check
			List<TicketRequest> requests = storage.getRequests();
			
			assertNotNull(requests);
			assertEquals(1, requests.size());
			assertEquals(r,requests.get(0));
			
			List<TicketResponse> responses = requests.get(0).getResponses();
			assertNotNull(responses);
			assertEquals(1,responses.size());
			assertEquals(response, responses.get(0));
			

		} catch (GetRequestsException | AddRequestException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} 
	}
	
	
	/**
	 * Test method for {@link org.nqc.bugme.data.storage.ObjectDBStorage#addRequest(org.nqc.bugme.data.TicketRequest)}.
	 */
	@Test
	public void testAddRequest() {
		
		ObjectDBStorage storage = new ObjectDBStorage(TEMP_STORAGE_DB);
		try {
			TicketRequest r = new TicketRequest("Storage1", "This is the first storage", new PersonID("Ionel"));
			storage.addRequest(r);
			
			// now check
			List<TicketRequest> requests = storage.getRequests();
			
			assertNotNull(requests);
			assertEquals(1, requests.size());
			assertEquals(r,requests.get(0));
			
		} catch (AddRequestException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (GetRequestsException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	
	/**
	 * test filtering of requests
	 */
	@Test
	public void testFilterRequests() {
		final String FILTER_MATCHING_TITLE = "Storage"; // the title to be stored
		
		// this criteria checks that the title is "Storage"
		TicketRequestQueryCriteria criteria = new TicketRequestQueryCriteria() {
			@Override
			public boolean matches(TicketRequest target) {
				return target!=null && FILTER_MATCHING_TITLE.equals(target.getTitle());
			}
		};
		
		
		// add some requests
		ObjectDBStorage storage = new ObjectDBStorage(TEMP_STORAGE_DB);
		try {
			TicketRequest r = new TicketRequest(FILTER_MATCHING_TITLE, "This is the first storage", new PersonID("Ionel"));
			storage.addRequest(r); 
			TicketRequest r2 = new TicketRequest(FILTER_MATCHING_TITLE + " that does not match", "This is the second storage", new PersonID("Gigel"));
			storage.addRequest(r2);
			
			// now check
			List<TicketRequest> requests = storage.getRequests();
			assertNotNull(requests);
			assertEquals(2, requests.size()); // we added 2 requests in total
			
			List<TicketRequest> filteredRequests = storage.getRequests(criteria);
			assertNotNull(filteredRequests);
			assertEquals(1, filteredRequests.size());  // only one matches the criteria
			
		} catch (AddRequestException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (GetRequestsException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		
	}

	
	
	
	
	/**
	 * Test method for {@link org.nqc.bugme.data.storage.ObjectDBStorage#addRequest(org.nqc.bugme.data.TicketRequest)}.
	 */
	@Test
	public void testDeleteRequest() {
		
		ObjectDBStorage storage = new ObjectDBStorage(TEMP_STORAGE_DB);
		try {
			TicketRequest r = new TicketRequest("Storage1", "This is the first storage", new PersonID("Ionel"));
			storage.addRequest(r);
			
			// now check
			List<TicketRequest> requests = storage.getRequests();
			
			assertNotNull(requests);
			assertEquals(1, requests.size());
			
			storage.removeRequest(requests.get(0));
			requests = storage.getRequests();
			assertNotNull(requests);
			assertEquals(0, requests.size());
			
			
		} catch (AddRequestException e) {
			e.printStackTrace();
			fail(e.getMessage());
		} catch (GetRequestsException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
}
