package org.nqc.bugme.data.storage.network;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nqc.bugme.data.TicketRequest;

public class RequestAddedNetworkMessageTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAddedRequest(){
		TicketRequest tr = new TicketRequest();
		RequestAddedNetworkMessage networkMessage = new RequestAddedNetworkMessage(tr);
		assertEquals(tr,networkMessage.getItem());
	}
}
