package org.nqc.bugme.data.storage.network;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NetworkMessageTest.class, RequestAddedNetworkMessageTest.class})
public class NetworkTestSuite {

}
