/**
 * 
 */
package org.nqc.bugme.data.storage.network;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author adch
 *
 */
public class NetworkMessageTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.nqc.bugme.data.storage.network.NetworkMessage#getLocalIP()}.
	 */
	@Test
	public void testGetLocalIP() {
		NetworkMessage msg = new NetworkMessage() {

			@Override
			public String getMessageSource() {
				return getLocalIP();
			}
			
		};
		
		String source = msg.getMessageSource();
		assertNotNull("Local address should NEVER be null",source);
		assertNotEquals("Returning 127.0.0.1 is an error (are you even connected to the network?)", "127.0.0.1", source);
	}

}
