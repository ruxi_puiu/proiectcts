/**
 * 
 */
package org.nqc.bugme.data.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.TicketResponse;
import org.nqc.bugme.data.storage.query.VisionMETestRequest;

/**
 * @author adch
 *
 */
public class InMemoryTestStorageTest {

	private TicketStorage storage;

	@Before
	public void setUp() {
		storage = new InMemoryStorage();
	}

	/**
	 * Test method for
	 * {@link org.nqc.bugme.data.storage.InMemoryStorage#addRequest(org.nqc.bugme.data.TicketRequest)}
	 * .
	 */
	@Test
	public void testAddRequest() {
		try {
			storage.addRequest(new TicketRequest());
			assertNotNull("The list of stored requests should not be null",
					storage.getRequests());
			assertTrue(storage.getRequests().size() == 1);
		} catch (AddRequestException | GetRequestsException e) {
			fail(e.getMessage());

		}
	}

	/**
	 * Test method for
	 * {@link org.nqc.bugme.data.storage.InMemoryStorage#addResponse(org.nqc.bugme.data.TicketResponse)}
	 * .
	 */
	@Test
	public void testAddTestResponse() {
		try {

			TicketRequest request = new TicketRequest();
			storage.addRequest(request);
//			storage.addResponse(new TicketResponse(request, null, "", "",
//					true));
//			assertNotNull("The list of stored responses should not be null",
//					storage.getResponses(request));
//			assertTrue(storage.getResponses(request).size() == 1);

		} catch (AddRequestException  e) {
			fail(e.getMessage());
		}
	}

	/**
	 * just test that we can add derived classes to the storage without it
	 * blowing up
	 */
	public void testAddDerivedRequestClasses() {

		try {
			storage.addRequest(new VisionMETestRequest(null));
		} catch (Exception e) {
			fail("Storage blew up with " + e.getClass().getName() + ":"
					+ e.getMessage()
					+ " when trying to add derived TicketRequest classes");
		}

	}

}
