/**
 * 
 */
package org.nqc.bugme.data.storage.query;

import org.nqc.bugme.data.PersonID;
import org.nqc.bugme.data.TicketRequest;

/**
 * just some random child of {@link TicketRequest}, to prove that the storage can also hold derived classes
 * @author adch
 *
 */
public class VisionMETestRequest extends TicketRequest {

	private static final long serialVersionUID = 1L;

	public VisionMETestRequest(PersonID person) {
		super("VisionME test request","Please test VisionME for me",person);
	}
	
	
}
