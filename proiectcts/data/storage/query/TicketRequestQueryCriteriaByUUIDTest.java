package org.nqc.bugme.data.storage.query;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.storage.GetRequestsException;
import org.nqc.bugme.data.storage.InMemoryStorage;

public class TicketRequestQueryCriteriaByUUIDTest {
	private RequestQueryCriteriaByUUID query;
	private TicketRequest tr;
	private InMemoryStorage storage;
	private ArrayList<TicketRequest> requests;

	@Before
	public void setUp() throws Exception {
		tr = new TicketRequest();
		query = new RequestQueryCriteriaByUUID(tr.getId());
		storage = new InMemoryStorage();
		storage.addRequest(tr);
		requests = new ArrayList<>();
	}

	@Test
	public void test() {
		try {
			requests = (ArrayList<TicketRequest>) storage.getRequests(query);
			assertTrue(requests.size() == 1);
		} catch (GetRequestsException e) {
			fail(e.getMessage());
		}
	}

}
