package org.nqc.bugme.data.storage.query;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ TicketRequestQueryCriteriaByUUIDTest.class})
public class QueryTestSuite {

}
