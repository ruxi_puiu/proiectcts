/**
 * 
 */
package org.nqc.bugme.data.storage;

import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.storage.output.ConsoleStorageListener;

/**
 * A console storage listener with a counter, used for testing.
 * @author adch
 *
 */
public class CountingConsoleTestStorageListener extends
		ConsoleStorageListener {

	private static int count = 0;
	
	
	/**
	 * @return the count
	 */
	public static int getCount() {
		return count;
	}
	
	
	@Override
	public void requestAdded(TicketRequest request) {
		count++;
		super.requestAdded(request);

	}

	@Override
	public void requestRemoved(TicketRequest request) {
		count--;
		super.requestRemoved(request);
	}

	
}
