package org.nqc.bugme.data.storage;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.storage.input.ConsoleStorageInput;
import org.nqc.bugme.data.storage.input.TicketStorageInput;

public class ConsoleTestStorageListenerTest {
	private CountingConsoleTestStorageListener listener;
	private InMemoryStorage storage;
	private TicketStorageInput input;

	@Before
	public void setUp() throws Exception {
		listener = new CountingConsoleTestStorageListener();
		storage = new InMemoryStorage();
		storage.addListener(listener);
		input = new ConsoleStorageInput();
		input.setTargetTestStorage(storage);

	}

	@Test
	public void testListenerCount() {
		for (int i = 1; i <= 5; i++) {
			listener.requestAdded(new TicketRequest());
		}
		assertTrue(CountingConsoleTestStorageListener.getCount() == 5);

	}

	@Test
	public void testListenersOnMultipleThreads() {
		DummyThreadedTestStorageInput[] inputs = new DummyThreadedTestStorageInput[50];
		
		// start the dummy inputs
		for (int i = 0; i < inputs.length; i++) {
			inputs[i] = new DummyThreadedTestStorageInput();
			inputs[i].setTargetTestStorage(storage);
			inputs[i].start();
		}
		
		
		// wait for the dummy inputs to stop
		for (int i = 0; i < inputs.length; i++) {
			inputs[i].waitToStop();
		}
		
		// now see that all inputs have done their work
		assertTrue(CountingConsoleTestStorageListener.getCount() == inputs.length+5);

	}
}
