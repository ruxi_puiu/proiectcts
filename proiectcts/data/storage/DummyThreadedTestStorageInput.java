/**
 * 
 */
package org.nqc.bugme.data.storage;

import org.nqc.bugme.data.PersonID;
import org.nqc.bugme.data.TicketRequest;
import org.nqc.bugme.data.storage.input.TicketStorageInput;

/**
 * generate some random data, using a thread; this input waits a random time,
 * then enters a single request and stops
 * 
 * @author adch
 *
 */
public class DummyThreadedTestStorageInput implements TicketStorageInput {

	private boolean started = false;
	private TicketStorage storage;
	private Thread inputThread;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.nqc.bugme.data.storage.input.TicketStorageInput#setTargetTestStorage
	 * (org.nqc.bugme.data.storage.TicketStorage)
	 */
	@Override
	public void setTargetTestStorage(TicketStorage target) {
		this.storage = target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.nqc.bugme.data.storage.input.TicketStorageInput#start()
	 */
	@Override
	public void start() {
		started = true;
		inputThread = new Thread(new Runnable() {

			@Override
			public void run() {

				String title;
				String description;
				PersonID myself = new PersonID();

				// wait some time, 0 to 5 seconds
				try {
					Thread.sleep((long)(Math.random()*5000.0));
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
				// just insert some dummy data here
				title = "Title" + Math.random();
				description = "Description" + Math.random();
				TicketRequest tr = new TicketRequest(title, description, myself);

				try {
					storage.addRequest(tr);
				} catch (AddRequestException e) {
					e.printStackTrace();
				}

				started = false;

			}
		});

		inputThread.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.nqc.bugme.data.storage.input.TicketStorageInput#stop()
	 */
	@Override
	public void stop() {
		// inputThread.interrupt();
		started = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.nqc.bugme.data.storage.input.TicketStorageInput#isStarted()
	 */
	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public void waitToStop() {
		if (inputThread != null && inputThread.isAlive()) {
			try {
				inputThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
