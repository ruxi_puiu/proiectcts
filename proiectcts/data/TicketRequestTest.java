/**
 * 
 */
package org.nqc.bugme.data;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nqc.bugme.data.TicketRequest.State;

/**
 * @author Administrator
 *
 */
public class TicketRequestTest {

	private static final String COMENTARIU = "comentariu";
	private static final String TITLU = "titlu";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.nqc.bugme.data.TicketRequest#TestRequest(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testConstructorWithTitleAndComments() {
		try {
			PersonID p = new PersonID();
			TicketRequest r = new TicketRequest(TITLU, COMENTARIU, p);
			assertEquals("Error when set the title!", TITLU, r.getTitle());
			assertEquals("Error when set the comment!", COMENTARIU,
					r.getComment());
			assertEquals("Error when set the PersonID", p, r.getPerson());

		} catch (IllegalArgumentException e) {

		}

	}

	/**
	 * Test method for {@link org.nqc.bugme.data.TicketRequest#getComment()}.
	 */
	@Test
	public void testImplicitConstructor() {
		TicketRequest r = new TicketRequest();
		assertNotNull(r.getTitle());
		assertNotNull(r.getComment());
		assertNotNull(r.getId());
		assertNotNull(r.getPerson());
	}

	@Test
	public void testConstructorWithCommentOnly() {
		try {
			new TicketRequest(null, COMENTARIU, new PersonID());
			fail("Test request accepts null title.");
			fail("Test request accepts null PersonID.");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testConstructorWithTitleOnly() {
		try {
			 new TicketRequest(TITLU, null, null);
			fail("Test request accepts null comment.");
			fail("Test request accepts null PersonID.");
		} catch (IllegalArgumentException e) {

		}

	}

	public void testConstructorWithPersonIDOnly() {
		try {
			new TicketRequest(null, null,new PersonID());
			fail("Test request accepts null comment.");
			fail("Test request accepts null title.");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testEquals() {
		assertNotEquals(new TicketRequest(), new TicketRequest());
	}
	
	
	@Test
	public void stateOfRequestWithNoResponsesShouldBeNotResponded(){
		TicketRequest r = new TicketRequest();
		assertEquals(r.getState(), State.NOT_RESPONDED);
	}
	
	
	@Test
	public void stateOfRequestWithNoAcceptedResponsesShouldBeInProgress(){
		TicketRequest r = new TicketRequest();
		TicketResponse resp = new TicketResponse();
		resp.setTestPassed(false);
		r.addResponse(resp);
		assertEquals(r.getState(), State.IN_PROGRESS);
	}
	
	
	@Test
	public void stateOfRequestWithAcceptedLastResponsesShouldBeComplete(){
		TicketRequest r = new TicketRequest();
		TicketResponse resp = new TicketResponse();
		resp.setTestPassed(false);
		r.addResponse(resp);
		
		TicketResponse resp2 = new TicketResponse();
		resp2.setTestPassed(true);
		r.addResponse(resp2);
		
		assertEquals(r.getState(), State.COMPLETE);
	}
	
	
	@Test
	public void stateOfRequestWithAcceptedInnerResponsesShouldBeBroken(){
		TicketRequest r = new TicketRequest();
		TicketResponse resp = new TicketResponse();
		resp.setTestPassed(false);
		r.addResponse(resp);
		
		TicketResponse resp2 = new TicketResponse();
		resp2.setTestPassed(true);
		r.addResponse(resp2);
		
		TicketResponse resp3 = new TicketResponse();
		resp3.setTestPassed(false);
		r.addResponse(resp3);
		
		assertEquals(r.getState(), State.BROKEN);
	}

}
