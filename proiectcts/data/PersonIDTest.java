/**
 * 
 */
package org.nqc.bugme.data;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author dexter
 *
 */
public class PersonIDTest {

	private static final String ID = "id";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testConstructor() {
		try {
			PersonID p = new PersonID(ID);
			assertNotNull(p.getId());
			assertEquals("Error when set the PersonId", ID, p.getPersonId());
		} catch (IllegalArgumentException e) {

		}

	}
	
	
	@Test
	public void testConstructorWithNullParameter() {
		try {
			new PersonID(null);
			fail("PersonId accepts null id");
		} catch (IllegalArgumentException e) {

		}

	}
	

	@Test
	public void testImplicitConstrustor() {
		PersonID p = new PersonID();
		assertNotNull(p.getId());
		assertNotNull(p.getPersonId());
	}

	@Test
	public void testEquals() {
		assertNotEquals(new PersonID(), new PersonID());
	}

}
