package org.nqc.bugme.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PersonIDTest.class, TicketRequestTest.class, TicketResponseTest.class})
public class DataTestSuite {

}
