package org.nqc.bugme.data;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JRViewer;

public class JasperReportsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JRException, InterruptedException {
		Map<String, Object> peopleMap = new HashMap<>();
        peopleMap.put("17",new TicketRequest("t1","c1",new PersonID("p1")) );
        peopleMap.put("18",new TicketRequest("t2","c2",new PersonID("p2")) );
        peopleMap.put("19",new TicketRequest("3","c3",new PersonID("p3")) );
        peopleMap.put("20",new TicketRequest("t4","c4",new PersonID("p4")) );
        peopleMap.put("21",new TicketRequest("t5","c5",new PersonID("p5")) );

        ArrayList<TicketRequest> dataList = new ArrayList<TicketRequest>();

        for(Map.Entry<String, Object> personMap : peopleMap.entrySet()) {
            TicketRequest person = new TicketRequest();
            person = (TicketRequest) personMap.getValue();
            dataList.add(person);
        }

        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map parameters = new HashMap();
        parameters.put("INFO", "Hello");

        
        JasperReport report = (JasperReport) JRLoader.loadObject(ClassLoader.getSystemClassLoader().getResourceAsStream("org/nqc/bugme/main/request.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, beanColDataSource);

        JFrame frame = new JFrame("Report");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new JRViewer(jasperPrint));
        frame.pack();
        frame.setVisible(true);
        
        
        Thread.sleep(120000);
	}

}
