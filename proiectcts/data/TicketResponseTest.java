package org.nqc.bugme.data;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TicketResponseTest {

	private static final String TITLE = "title";
	private static final String COMMENT = "comment";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testConstructor() {
		TicketRequest request = new TicketRequest();
		TicketResponse tr = new TicketResponse(request, null, COMMENT, TITLE, true);
		assertEquals("Eroare la setare TicketRequest", request, tr.getRequest());
		assertEquals("Eroare la setare titlu", TITLE, tr.getTitle());
		assertEquals("Eroare la setare comment", COMMENT, tr.getComment());

	}

	@Test
	public void testImplicitConstructor() {
		TicketResponse tr = new TicketResponse();
		assertNotNull(tr.getRequest());
		assertNotNull(tr.getTitle());
		assertNotNull(tr.getComment());

	}

	@Test
	public void testConstructorWirhTestRequestOnly() {
		try {
			new TicketResponse(new TicketRequest(), null, null, null, false);
			fail("TicketResponse accepts null TicketResponse");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testConstructorWirhTitleOnly() {
		try {
			new TicketResponse(null, null,TITLE, null, false);
			fail("TicketResponse accepts null title");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testConstructorWirhCommentOnly() {
		try {
			new TicketResponse(null, null,null, COMMENT, false);
			fail("TicketResponse accepts null title");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testNotEquals() {
		assertNotEquals(new TicketResponse(), new TicketResponse());
	}

}
